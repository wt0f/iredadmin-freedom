
This is a reimplementation of the iRedAdmin-Pro control panel. It has been
given the name iRedAdmin-Freedom as it is meant to unleash the power under
the hood of the iRedMail system.

It has been created from a forked copy of the
[iRedAdmin](https://github.com/iredmail/iRedAdmin) repository.

* Please report bugs in the issue tracker:
  https://gitlab.com/wt0f/iredadmin-freedom/-/issues

* Merge requests to address bugs or to add functionally are welcome.
